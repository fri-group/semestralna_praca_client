
#ifndef SEMKA_TIC_TAC_TOE_CLIENT_MAIN_H
#define SEMKA_TIC_TAC_TOE_CLIENT_MAIN_H

#endif //SEMKA_TIC_TAC_TOE_CLIENT_MAIN_H

void initHraciePole();
void vykresli();
void odosliServru(int kod);
void zaver(int vyhralHrac);
void * hlavneVlakno(void * parametre);
void * klientSocketVlakno(void * parametre);

