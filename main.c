#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <ctype.h>

typedef struct hlavne {
    pthread_mutex_t * mut;
    pthread_cond_t * spravTahKlient;
    pthread_cond_t * pokracujTahKlient;
} HLAVNE;

typedef struct klientSocket {
    pthread_mutex_t * mut;
    pthread_cond_t * spravTahKlient;
    pthread_cond_t * pokracujTahKlient;
} KLIENTSOCKET;

/// vlakna
pthread_t klientSocket;
pthread_t hlavne;

int hraciaPlocha[25];
int beziHra = 0;
int tahKlient = 0;
int response = 0;   // premenna na ulozenie odpovede z overenia pozicie
int vyhral = 0;     // nastavi sa na 1 alebo 2 ak niekto vyhral

/// komunikacia
int sockfd, n;
char buffer[256];

void initHraciePole() {
    for (int i = 0; i < 25; ++i) {
        hraciaPlocha[i] = 0;
    }
}

void vykresli() {
    if (beziHra == 1) {
        for (int i = 0; i < 25; ++i) {
            if (i % 5 == 0) {
                printf("\n| ");
            }
            if (hraciaPlocha[i] == 1) {
                printf(" X | ");
            } else if (hraciaPlocha[i] == -1) {
                printf(" O | ");
            } else {
                if (i < 10) {
                    printf(" %d | ", i);
                } else {
                    printf("%d | ", i);
                }
            }
        }
    } else {    // zaverecny-bez pomocnych cisel
        usleep(5000);
        for (int i = 0; i < 25; ++i) {
            if (i % 5 == 0) {
                printf("\n| ");
            }
            if (hraciaPlocha[i] == 1) {
                printf(" X | ");
            } else if (hraciaPlocha[i] == -1) {
                printf(" O | ");
            } else {
                printf("   | ");
            }
        }
    }
    printf("\n");
}

void odosliServru(int kod) {
    if (kod != 0) {
        bzero(buffer, 256);
        sprintf(buffer, "%d", kod);
    }
    n = write(sockfd, buffer, strlen(buffer));
    if (n < 0) {
        perror("Chyba zapisovania socketu!!");
    }
}

void zaver(int vyhralHrac) {
    if (vyhralHrac == 2) {
        vyhral = 2;
    } else if (vyhralHrac == 1) {
        vyhral = 1;
        odosliServru(99); // odoslanie kodu 99 - koniec
    } else if (vyhralHrac == 3) {
        vyhral = 3;
        odosliServru(99);
    }
    tahKlient = 1;
    beziHra = 0;
    vykresli();
}

void * hlavneVlakno(void * parametre)  {
    HLAVNE * data = (HLAVNE *) parametre;
    initHraciePole();
    vykresli();

    while (beziHra == 1) {
        pthread_mutex_lock(data->mut);
        while (tahKlient == 0) {
            printf("\nČaká sa na ťah servera.\n");
            pthread_cond_wait(data->spravTahKlient, data->mut);
        }
        if (beziHra == 0) {
            break;
        }
        tahKlient = 0;
        int pokr = 0;
        while (pokr == 0) {
            printf("Zadaj pozíciu alebo príkaz: ");
            bzero(buffer, 256);
            fgets(buffer, 255, stdin);

            if(!(isdigit(buffer[0]))) {
                printf("Zadal si neplatný znak!\n");
                continue;
            }

            int prikaz = atoi(buffer);

            if (prikaz >= 0 && prikaz <= 24) {
                char overenie[256];
                bzero(overenie, 256);
                sprintf(overenie, "%d", 49);
                n = write(sockfd, overenie, strlen(buffer)); // odoslanie kodu 49 - aby server overeil poziciu
                if (n < 0) {
                    perror("Chyba zapisovania socketu!!");
                }

                usleep(500);
                response = 0;
                odosliServru(0); // odoslanie pozicie

                while (response == 0) {
                    printf("Overenie... ");
                    pthread_cond_wait(data->pokracujTahKlient, data->mut);
                }
                if (response == 50) {
                    /// ok
                    printf("OK\n");
                    int pozicia = prikaz;
                    hraciaPlocha[pozicia] = -1; // zaznamenanie na klientovi
                    vykresli();
                    pokr = 1;
                } else if (response == 51) {
                    /// obsadene
                    printf("Chyba\n");
                    printf("Políčko je už obsadené. Zvoľ iné!\n");
                }
            } else if (prikaz == 99) {
                /// koniec
                printf("Hra sa končí...\n");
                odosliServru(99); // odoslanie kodu 99 - koniec
                tahKlient = 1;
                beziHra = 0;
                break;
            } else if (prikaz == 95) {
                /// restart
                odosliServru(95); // odoslanie kodu 95 - ziadost o restart
                initHraciePole();
                vykresli();
                pokr = 1;
            } else {
                printf("Neplatný príkaz!\n");
            }
            pthread_mutex_unlock(data->mut);
        }
    }
    return NULL;
}

void * klientSocketVlakno(void * parametre)  {
    KLIENTSOCKET * data = (KLIENTSOCKET *) parametre;

    while (beziHra == 1) {
        bzero(buffer, 256);
        n = read(sockfd, buffer, 255);
        if (n < 0) {
            perror("Chyba čítania socketu");
        }
        int prijataHodnota = atoi(buffer);

        if (prijataHodnota >= 0 && prijataHodnota <= 24) {
            /// zaznamenanie policka ktore zadal server
            pthread_mutex_lock(data->mut);
            int pozicia = prijataHodnota;
            hraciaPlocha[pozicia] = 1;
            printf("\nServer označil pozíciu %d. \n", pozicia);
            vykresli();
            tahKlient = 1;
            pthread_cond_signal(data->spravTahKlient);
            pthread_mutex_unlock(data->mut);
        } else if (prijataHodnota == 99) {
            /// koniec
            printf("Server ukončil hru...\n");
            tahKlient = 1;
            beziHra = 0;
            pthread_cond_signal(data->spravTahKlient);
        } else if (prijataHodnota == 98) {
            /// klient vyhral
            zaver(2);
            pthread_cond_signal(data->spravTahKlient);
        } else if (prijataHodnota >= 60 && prijataHodnota <= 84) {
            /// klient prehral / remiza
            printf("Server označil pozíciu %d.\n", prijataHodnota - 60);
            hraciaPlocha[prijataHodnota - 60] = 1; // oznacenie posledneho tahu ktory spravil server

            bzero(buffer, 256);
            n = read(sockfd, buffer, 255);
            if (n < 0) {
                perror("Chyba čítania socketu");
            }
            prijataHodnota = atoi(buffer);
            if (prijataHodnota == 1) { // vyhral server
                zaver(1);
            } else if (prijataHodnota == 3) { // remiza
                zaver(3);
            }
            pthread_cond_signal(data->spravTahKlient);
        } else if (prijataHodnota == 95) {
            /// restart
            printf("Server požiadal o reštart.\n");
            initHraciePole();
            vykresli();
            printf("\nČaká sa na ťah servera.\n");
        } else if (prijataHodnota == 50 || prijataHodnota == 51) {
            /// odpoved na overenie
            response = prijataHodnota;
            pthread_cond_signal(data->pokracujTahKlient);
        }
    }
    return NULL;
}

int main(int argc, char * argv[]) {
    struct sockaddr_in serv_addr;
    struct hostent* server;

    if (argc < 3) {
        fprintf(stderr, "Použitie %s ipadresa_servera port!\n", argv[0]);
        return 1;
    }

    server = gethostbyname(argv[1]);
    if (server == NULL) {
        fprintf(stderr, "Taky server mi je neznamy!\n");
        return 2;
    }

    bzero((char*)&serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy(
            (char*)server->h_addr,
            (char*)&serv_addr.sin_addr.s_addr,
            server->h_length
    );
    serv_addr.sin_port = htons(atoi(argv[2]));

    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) {
        perror("Nepodarilo sa vytvorit socket!!");
        return 3;
    }

    if (connect(sockfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0) {
        perror("Chyba pripojenia !!");
        return 4;
    }

    beziHra = 1;

    printf("~~~~ Tic-tac-toe ~~~~\n");
    printf("~~~ Lenka Samcová ~~~\n");
    printf("\n");
    printf("Tvoj znak je O\n");
    printf("Označenie pozície: <0,24>\n");
    printf("Reštart: 95\n");
    printf("Koniec: 99\n");

    pthread_mutex_t mut;
    pthread_cond_t spravTahKlient, pokracujTahKlient;
    pthread_mutex_init(&mut, NULL);
    pthread_cond_init(&spravTahKlient, NULL);
    pthread_cond_init(&pokracujTahKlient, NULL);

    HLAVNE dataHlavneVlakno = {&mut, &spravTahKlient, &pokracujTahKlient};
    KLIENTSOCKET dataKlientSocket = {&mut, &spravTahKlient, &pokracujTahKlient};

    pthread_create(&hlavne, NULL, &hlavneVlakno, &dataHlavneVlakno);
    pthread_create(&klientSocket, NULL, &klientSocketVlakno, &dataKlientSocket);

    pthread_join(hlavne, NULL);
    pthread_join(klientSocket, NULL);

    if (vyhral == 2) {
        printf("Vyhral si!\n");
    } else if (vyhral == 1) {
        printf("Prehral si. Server bol lepší!\n");
    } else if (vyhral == 3) {
        printf("Remiza! Už nie je možný ďalší ťah.\n");
    }

    // clean up
    pthread_cond_destroy(&spravTahKlient);
    pthread_cond_destroy(&pokracujTahKlient);
    pthread_mutex_destroy(&mut);

    close(sockfd);
    return 0;
}
